import os
import csv
import subprocess

# retrieve environment variable with the logical group name inside
relevant_logical_group = os.environ["TERMINOLOGY_LOGICAL_GROUP"]

# get all the changed terminologies as relative paths
with open("terminology_file_paths.txt", "r") as file:
    changed_file_paths_as_lines = file.readlines()

# get all the terminologies as relative paths
with open("all_terminology_file_paths.txt", "r") as file:
    file_paths_as_lines = file.readlines()

# read the logical groups into a dict with the terminology name as key
reader = csv.reader(open("terminologies/term_to_logical_groups.csv"))
term_to_logical_groups = {rows[0]:rows[1] for rows in reader}

# create a list with the changed terminologies of the relevant logical group
relevant_logical_group_having_changed_terms = []
for one_changed_file_path_line in changed_file_paths_as_lines:
    # check if after removing \n there is still a string left
    if file_path_line := one_changed_file_path_line.strip("\n"):
        # retrieve name of terminology (e.g. CodeSystem-appc-anatomie) without file extension
        term_name = os.path.basename(file_path_line).split('.')[0]
        if (the_lg := term_to_logical_groups.get(term_name)) and the_lg == relevant_logical_group:
            # for differiantal runs, activate following line of code
            #relevant_logical_group_having_changed_terms.append(file_path_line)
            # IGNORE DIFF RUNS following three lines of code will ignore the differiantial runs (only processing changed terminologies) and add all terminologies from a group, if one terminology has changed
            all_keys_in_group = [k for k,v in term_to_logical_groups.items() if (the_lg := v) and the_lg == relevant_logical_group]
            relevant_logical_group_having_changed_terms = [a_file_path_line for a_file_path_line in file_paths_as_lines if os.path.basename(a_file_path_line.strip("\n")).split('.')[0] in all_keys_in_group]
            break

os.rename("terminology_file_paths.txt", "terminology_file_paths_without_grouping.txt")
print("Old terminology_file_paths.txt renamed to terminology_file_paths_without_grouping.txt.")
with open("terminology_file_paths.txt", "w") as file:
    print("New terminology_file_paths.txt created with individual logical groups per run.")
    terminology_changed = False

    # if the group is used in the changed terminologies or input/resources, overwrite the file with only the terminologies from the group
    for file_path_line in relevant_logical_group_having_changed_terms:
        terminology_changed = True
        dir_name = os.path.dirname(file_path_line)
        # retrieve name of terminology (e.g. CodeSystem-appc-anatomie) without file extension
        term_name = os.path.basename(file_path_line).split('.')[0]

        # right now there are only two ways to have terminologies processed, so TRUE would mean its inside the terminologies folder and FALSE inside the input/resources
        if dir_name.startswith('terminologies/'):
            file_path_line = dir_name + '/' + term_name + '.4.fhir.json'
        file.write(file_path_line + "\n")

    # if the group is "_all_other_terminologies", then delete all lines in the file paths that are in a group
    if relevant_logical_group == "_all_other_terminologies":
        terminology_changed = True
        all_other_therminologies = set()
        # IGNORE DIFF RUNS reuse changed_file_paths_as_lines instead of file_paths_as_lines to activate diff runs
        for one_file_path_line in file_paths_as_lines:#changed_file_paths_as_lines:
            if file_path_line := one_file_path_line.strip("\n"):
                dir_name = os.path.dirname(file_path_line)
                # retrieve name of terminology (e.g. CodeSystem-appc-anatomie) without file extension
                term_name = os.path.basename(file_path_line).split('.')[0]

                if term_name not in term_to_logical_groups:
                    # right now there are only two ways to have terminologies processed, so TRUE would mean its inside the terminologies folder and FALSE inside the input/resources
                    if dir_name.startswith('terminologies/'):
                        file_path_line = dir_name + '/' + term_name + '.4.fhir.json'
                    #file.write(file_path_line + "\n")
                    # IGNORE DIFF RUNS add to eliminate duplicates
                    all_other_therminologies.add(file_path_line)
                    
        # IGNORE DIFF RUNS
        for a_terminology in sorted(all_other_therminologies):
            file.write(a_terminology + "\n")

    # if the group is "__no_terminologies", then delete everything
    if relevant_logical_group == "__no_terminologies":
        terminology_changed = True
        file.write("")

    # if no terminology has been changed from the logical group, exit with a warning (exit 3)
    if not terminology_changed:
        import sys
        print("No terminology has been changed from the logical group TERMINOLOGY_LOGICAL_GROUP='"+relevant_logical_group+"'.", file=sys.stderr)
        exit(3)