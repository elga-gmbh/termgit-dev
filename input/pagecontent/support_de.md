> **For the english version please click [here](support_en.md).**

Bitte wählen Sie je nach dem Thema der angeforderten Unterstützung einen der folgenden Kanäle.

### Technische Angelegenheiten
Verbesserungsvorschläge oder Fehlermeldungen, die bei der Anwendung von **TerminoloGit** erkannt wurden, können über das GitLab-Ticketsystem unter **[https://gitlab.com/elga-gmbh/termgit-dev/-/issues/new](https://gitlab.com/elga-gmbh/termgit-dev/-/issues/new)** eingemeldet werden.

### Terminologien
Alle mit dieser Dokumentation nicht beantwortbaren Fragen zu **Terminologien** können an **[cda@elga.gv.at](mailto:cda@elga.gv.at)** gemeldet werden.

### ELGA
Bei Fragen zu Ihrem ELGA und ELGA-Portal wenden Sie sich bitte an **[info@elga-serviceline.at](mailto:info@elga-serviceline.at)** oder **[+43 (0)50 124 4411](tel:+43501244411)**
(werktags von 7.00 - 19.00 Uhr).

### Impressum
Es gilt das Impressum von **[elga.gv.at/impressum-kontakt](https://www.elga.gv.at/impressum-kontakt/)**.

### Datenschutzerklärung
Es gilt die Datenschutzerklärung von **[elga.gv.at/datenschutzerklaerung](https://www.elga.gv.at/datenschutzerklaerung/)**.

### Lizenz und rechtliche Bedingungen

TerminoloGit ist lizenziert unter der GNU General Public License v3.0 oder höher - siehe die Datei [LICENSE.md](https://gitlab.com/elga-gmbh/termgit-dev/-/blob/stable/LICENSE.md) für Details.

#### HL7®

HL7®, HEALTH LEVEL SEVEN® und FHIR® sind Marken im Besitz von Health Level Seven International, eingetragen beim United States Patent and Trademark Office.

Dieser Implementierungsleitfaden enthält und verweist auf geistiges Eigentum, das Dritten gehört ("Third Party IP"). Mit der Annahme dieser Lizenzbedingungen werden keine Rechte in Bezug auf das geistige Eigentum Dritter gewährt. Der Lizenznehmer ist allein dafür verantwortlich, alle erforderlichen Lizenzen oder Genehmigungen für die Nutzung des geistigen Eigentums Dritter in Verbindung mit der Spezifikation oder auf andere Weise zu ermitteln und einzuholen.

#### IP Statements

Automatisch vom HL7® FHIR® IG Publisher extrahiert.

{% include ip-statements.xhtml %}
